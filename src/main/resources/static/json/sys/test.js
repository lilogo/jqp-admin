
AMIS_JSON={
    "type": "page",
    "body": [{
        "label": "树型展示",
        "type": "select",
        "name": "a",
        "multiple": false,
        "selectMode": "tree",
        "searchable": true,
        "options": [
            {
                "label": "法师",
                "children": [
                    {
                        "label": "诸葛亮",
                        "value": "zhugeliang"
                    }
                ]
            },
            {
                "label": "战士",
                "children": [
                    {
                        "label": "曹操",
                        "value": "caocao"
                    },
                    {
                        "label": "钟无艳",
                        "value": "zhongwuyan"
                    }
                ]
            },
            {
                "label": "打野",
                "children": [
                    {
                        "label": "李白",
                        "value": "libai"
                    },
                    {
                        "label": "韩信",
                        "value": "hanxin"
                    },
                    {
                        "label": "云中君",
                        "value": "yunzhongjun"
                    }
                ]
            }
        ]
    }]
}